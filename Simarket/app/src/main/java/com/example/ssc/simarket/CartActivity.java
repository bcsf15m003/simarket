package com.example.ssc.simarket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import database.AppDatabase;
import database.Product;
import database.cart;
import database.cartDao;

import static java.security.AccessController.getContext;

public class CartActivity extends Activity{

    private RecyclerView cartrecyclerView;
    private cartAdapter cAdapter;
    List<cart> cList=new ArrayList<>();


        @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_for_search);

        cartrecyclerView=(RecyclerView) findViewById(R.id.recycler_view);

         cartDao cDao = AppDatabase.getInstance(getApplicationContext()).cart_items();
           cList=cDao.getAllcartItems();

           if(!cList.isEmpty()) {
               cAdapter = new cartAdapter(getApplicationContext(),cList);
               cartrecyclerView.setAdapter(cAdapter);
               cartrecyclerView.setHasFixedSize(true);
               RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
               cartrecyclerView.setLayoutManager(mLayoutManager);

           }
           else
           {
               Toast.makeText(getApplicationContext(),"Cart List is empty",Toast.LENGTH_SHORT).show();
               Intent i=new Intent(this,MainActivity.class);
               i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(i);
           }




        }

       /* public void populateCartTable()
        {
            cart_table.removeAllViews();
            TableRow tbrow0 = new TableRow(getApplicationContext());
            TextView tv0 = new TextView(getApplicationContext());
            tv0.setText("Total Products.");
            tv0.setGravity(Gravity.CENTER);
            tbrow0.addView(tv0);

            TextView tv1 = new TextView(getApplicationContext());
            tv1.setText("Product Image");
            tv1.setGravity(Gravity.CENTER);
            tbrow0.addView(tv1);

            TextView tv2 = new TextView(getApplicationContext());
            tv2.setText("Product");
            tv2.setGravity(Gravity.CENTER);
            tbrow0.addView(tv2);

            TextView tv3 = new TextView(getApplicationContext());
            tv3.setText("Price");
            tv3.setGravity(Gravity.CENTER);
            tbrow0.addView(tv3);

            TextView tv4 = new TextView(getApplicationContext());
            tv4.setText("Quantity");
            tv4.setGravity(Gravity.CENTER);
            tbrow0.addView(tv4);

            TextView tv5 = new TextView(getApplicationContext());
            tv5.setText("Total");
            tv5.setGravity(Gravity.CENTER);
            tbrow0.addView(tv5);

            cart_table.addView(tbrow0);
            for (int i = 0; i < list_items.size(); i++) {

                cart c_item = list_items.get(i);
                TableRow tbrow = new TableRow(getApplicationContext());

                TextView t1v = new TextView(getApplicationContext());
                t1v.setText(c_item.getCart_id());
                t1v.setGravity(Gravity.CENTER);
                tbrow.addView(t1v);

                ImageView img_u=new ImageView(getApplicationContext());
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(70,70);
                img_u.setLayoutParams(parms);
                Glide.with(getApplicationContext()).load(c_item.getPro_image()).into(img_u);
                tbrow.addView(img_u);

                TextView t2v = new TextView(getApplicationContext());
                t2v.setText(c_item.getTitle());
                t2v.setGravity(Gravity.CENTER);
                tbrow.addView(t2v);

                TextView t3v = new TextView(getApplicationContext());
                t3v.setText(c_item.getPrice());
                t3v.setGravity(Gravity.CENTER);
                tbrow.addView(t3v);

                TextView t4v = new TextView(getApplicationContext());
                t4v.setText(c_item.getQuantity());
                t4v.setGravity(Gravity.CENTER);
                tbrow.addView(t4v);

                TextView t5v = new TextView(getApplicationContext());
                t5v.setText(c_item.getTotal());
                t5v.setGravity(Gravity.CENTER);
                tbrow.addView(t5v);

                cart_table.addView(tbrow);
            }
        }
*/

}
