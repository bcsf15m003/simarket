package com.example.ssc.simarket;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import database.cart;

import static com.example.ssc.simarket.cartAdapter.MY_PREFS_NAME;

public class PaymentActivity extends Activity{

   // String n,p,t,img,q;
    Button online,dilivery;
    ImageView image;
    TextView name,price,quantity,amount;
    Bundle b;
    int t;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_payment);


            Intent i = getIntent();
            b = i.getExtras();
            NumberFormat nm = NumberFormat.getNumberInstance();
            initVariables();


            //n = b.getString("IntentProductTitle");
            //   p = nm.format(b.getInt("IntentProductPrice"));
            // t = nm.format(b.getInt("IntentProductAmount"));
            //q = nm.format(b.getInt("IntentProductQuantity"));
            //img = b.getString("IntentProductImage");
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
           // String restoredText = prefs.getString("IntentProductTitle", null);
            //if (restoredText != null) {
                String n = prefs.getString("IntentProductTitle", "No name defined");//"No name defined" is the default value.
                int p = prefs.getInt("IntentProductPrice", 0); //0 is the default value.
                 t = prefs.getInt("IntentProductAmount", 0); //0 is the default value.
                int q = prefs.getInt("IntentProductQuantity", 0); //0 is the default value.
                String img = prefs.getString("IntentProductImage", "No name defined");

                name.setText(n);
                price.setText(Integer.toString(p));
                amount.setText(Integer.toString(t));
                quantity.setText(Integer.toString(q));

                Glide.with(getApplicationContext()).load(img).into(image);
                //prefs.edit().clear().commit();

            online.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), CheckoutActivity.class);
                    intent.putExtra("amount",t);
                    startActivity(intent);
                   // finish();
                }
            });

        }

        @Override
        public void finish()
        {
            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
         prefs.edit().clear().commit();
        }
    public void initVariables()
    {
        online=(Button)findViewById(R.id.btn_online);
        dilivery=(Button)findViewById(R.id.btn_dil);


        name=(TextView)findViewById(R.id.order_name);
        price=(TextView)findViewById(R.id.order_price);
        quantity=(TextView)findViewById(R.id.order_quantity);
        amount=(TextView)findViewById(R.id.order_total);
        image=(ImageView)findViewById(R.id.oimage);



    }
}
