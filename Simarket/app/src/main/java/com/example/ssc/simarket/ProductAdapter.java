package com.example.ssc.simarket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;


import java.util.List;

import database.Product;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<Product> ProductsList;
    private View v;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView name,price;


        public MyViewHolder(View view) {
            super(view);
            v=view;
            name = (TextView) view.findViewById(R.id._name);
            img = (ImageView) view.findViewById(R.id._image);

        }
    }

    public ProductAdapter(List<Product> ProList) {
        this.ProductsList = ProList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_searched_content, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Product p = ProductsList.get(position);
        holder.name.setText(p.getTitle());
        String url=p.getPro_image();
        Glide.with(v).load(url).into(holder.img);

    }

    @Override
    public int getItemCount() {
        return ProductsList.size();
    }
}
