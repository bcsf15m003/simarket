package com.example.ssc.simarket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import database.AppDatabase;
import database.Product;
import database.ProductDao;
import database.User;
import database.UserDao;

public class SignupActivity extends Activity {

    EditText fname,lname,country,street_addr,city,state,postCode,phone,email,password;
    Button signup_btn;
    TextView heading;
    Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);




        initVariables();
        Intent i=getIntent();
        b=i.getExtras();


        if(b.getBoolean("flag"))
        {
            email.setText(b.getString("uname"));
            password.setText(b.getString("password"));
            fname.setText(b.getString("fname"));
            lname.setText(b.getString("lname"));
            country.setText(b.getString("country"));
            city.setText(b.getString("city"));
            street_addr.setText(b.getString("street_add"));
            state.setText(b.getString("state"));
            postCode.setText(b.getString("postcode"));
            phone.setText(b.getString("phone_no"));


        }

        signup_btn.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                              User u = new User();
                                              u.setEmail(email.getText().toString());
                                              u.setPassword(password.getText().toString());
                                              u.setFname(fname.getText().toString());
                                              u.setLname(lname.getText().toString());
                                              u.setCountry(country.getText().toString());
                                              u.setCity(city.getText().toString());
                                              u.setPostcode(postCode.getText().toString());
                                              u.setPhone(phone.getText().toString());
                                              u.setStreet_add(street_addr.getText().toString());
                                              u.setState(state.getText().toString());


                                              SharedPreferences sp =  getSharedPreferences("User_session", Context.MODE_PRIVATE);


                                              SharedPreferences.Editor editor = sp.edit();
                                              editor.putString("uname",email.getText().toString() );
                                              editor.putString("pswd", password.getText().toString());
                                              editor.apply();

                                              UserDao udao = AppDatabase.getInstance(getApplicationContext()).user();

                                              if(b.getBoolean("flag"))
                                              {
                                                  udao.UpdateUser(u);
                                                  Toast.makeText(getApplicationContext(), "Account updated", Toast.LENGTH_SHORT).show();
                                              }
                                              else
                                              {
                                                  udao.insertUser(u);
                                                  Toast.makeText(getApplicationContext(), "Account created", Toast.LENGTH_SHORT).show();
                                              }

                                              Intent i = new Intent(getApplicationContext(), MeActivity.class);
                                              i.putExtra("flag",false);
                                              i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                              startActivity(i);
                                          }
                                      });







    }

    public void initVariables()
    {
        heading=(TextView) findViewById(R.id.tv_signup);
        signup_btn = (Button)findViewById(R.id.sign_up_btn);
        fname= (EditText)findViewById(R.id.firstName_s);
        lname = (EditText)findViewById(R.id.lastName_s);
        country = (EditText)findViewById(R.id.country_s);
        street_addr = (EditText)findViewById(R.id.address_s);
        city=(EditText)findViewById(R.id.town_city_s);
        state=(EditText)findViewById(R.id.state_s);
        postCode=(EditText)findViewById(R.id.zipcode_s);
        phone=(EditText)findViewById(R.id.phone_s);
        email=(EditText)findViewById(R.id.email_s);
        password =(EditText)findViewById(R.id.pasword_s);
    }
    public void getLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
