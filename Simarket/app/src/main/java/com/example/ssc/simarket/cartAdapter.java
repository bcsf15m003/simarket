package com.example.ssc.simarket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import database.AppDatabase;
import database.cart;
import database.cartDao;

import static android.content.Context.MODE_PRIVATE;

public class cartAdapter extends RecyclerView.Adapter<cartAdapter.MyViewHolder> {

        private List<cart> cartList;
        private Context context;
        private View v;
        public static final String MY_PREFS_NAME = "MyPrefsFile";
        public cartAdapter(Context c, List<cart> List) {
        this.cartList = List;
        this.context=c;


        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
        .inflate(R.layout.activity_cart, parent, false);

        return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            cart c = cartList.get(position);
            holder.item_no.setText(String.valueOf(c.getCart_id()));
            String url = c.getPro_image();
            Glide.with(v).load(url).into(holder.img);
            holder.ctitle.setText(String.valueOf(c.getTitle()));
            holder.price.setText(String.valueOf(c.getPrice()));
            holder.quantity.setText(String.valueOf(c.getQuantity()));
            holder.total.setText(String.valueOf(c.getTotal()));
            holder.del.setChecked(false);
            holder.pay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    cart cart = cartList.get(position);
                    Toast.makeText(context, cart.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString("IntentProductTitle", cart.getTitle());
                    editor.putInt("IntentProductPrice", cart.getPrice());
                    editor.putInt("IntentProductQuantity", cart.getQuantity());
                    editor.putInt("IntentProductAmount", cart.getTotal());
                    editor.putString("IntentProductImage", cart.getPro_image());
                    editor.apply();

                    Intent intent = new Intent(context, PaymentActivity.class);
                    intent.putExtra("IntentProductTitle", cart.getTitle());
                    intent.putExtra("IntentProductDescription", cart.getPro_des());
                    intent.putExtra("IntentProductPrice", cart.getPrice());
                    intent.putExtra("IntentProductImage", cart.getPro_image());
                    intent.putExtra("IntentProductQuantity", cart.getQuantity());
                    intent.putExtra("IntentProductAmount", cart.getTotal());
                    context.startActivity(intent);
                }
            });




        }
        @Override
        public int getItemCount() {
        if(cartList.isEmpty())
        {
            Intent i=new Intent(context,MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(i);
        }

        return cartList.size();
        }

    public class MyViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener{
        private ImageView img;
        private TextView item_no, ctitle, price, quantity, total;
        private CheckBox del;
        private Button pay;

        private MyViewHolder(View view) {
            super(view);
            v = view;
            item_no = (TextView) view.findViewById(R.id.c_item_no);
            img = (ImageView) view.findViewById(R.id.cimage);
            ctitle = (TextView) view.findViewById(R.id.cname);
            price = (TextView) view.findViewById(R.id.cprice);
            quantity = (TextView) view.findViewById(R.id.cquantity);
            total = (TextView) view.findViewById(R.id.ctotal);
            del = (CheckBox) view.findViewById(R.id.cdel);
            del.setOnCheckedChangeListener(this);
            pay = (Button) view.findViewById(R.id.cart_checkout);

        }
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            removeAt(getAdapterPosition(),this);
        }


    }

    private void removeAt(int position,MyViewHolder viewHolder) {

        cartDao cdao = AppDatabase.getInstance(context).cart_items();
        cdao.deleteCartItem(cartList.get(position));
        cartList.remove(position);
        notifyItemRemoved(position);
    }



        }

