package com.example.ssc.simarket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import database.AppDatabase;
import database.User;
import database.UserDao;


public class MeActivity  extends AppCompatActivity implements View.OnClickListener{

    RelativeLayout myprofile,my_cart,feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);

        myprofile=(RelativeLayout)findViewById(R.id.Rl1);
        my_cart=(RelativeLayout)findViewById(R.id.Rl2);
        feedback=(RelativeLayout)findViewById(R.id.Rl3);
        myprofile.setOnClickListener(this);
        my_cart.setOnClickListener(this);
        feedback.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.img_feedback || v.getId()==R.id.tv_feedback || v.getId()==R.id.Rl3)
        {
            Intent j = new Intent(this, FeedbackActivity.class);
            startActivity(j);
        }

        else if(v.getId()==R.id.img_cart || v.getId()==R.id.tv_cart  || v.getId()==R.id.Rl2)
        {
            Intent i = new Intent(this, CartActivity.class);
            startActivity(i);
        }
        else if(v.getId()==R.id.img_profile || v.getId()==R.id.tv_myprofile  || v.getId()==R.id.Rl1) {

            SharedPreferences sp = getSharedPreferences("User_session", Context.MODE_PRIVATE);

            String u = sp.getString("uname", null);
            String p = sp.getString("pswd", null);
            if (u != null && p != null) {

                UserDao uobj = (UserDao) AppDatabase.getInstance(getApplicationContext()).user();
                User user = uobj.getUser(u, p);

                if (user != null) {
                    Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                    intent.putExtra("flag", true);
                    intent.putExtra("fname", user.getFname());
                    intent.putExtra("lname", user.getLname());
                    intent.putExtra("uname", user.getEmail());
                    intent.putExtra("country", user.getCountry());
                    intent.putExtra("city", user.getCity());
                    intent.putExtra("street_add", user.getStreet_add());
                    intent.putExtra("state", user.getState());
                    intent.putExtra("postcode", user.getPostcode());
                    intent.putExtra("phone_no", user.getPhone());
                    intent.putExtra("password", user.getPassword());
                    startActivity(intent);
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "User Need to create Account to chk profile", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), SignupActivity.class);
                i.putExtra("flag",false);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        }

    }
}
