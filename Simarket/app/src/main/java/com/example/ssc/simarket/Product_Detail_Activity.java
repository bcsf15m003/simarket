package com.example.ssc.simarket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;

import database.AppDatabase;
import database.User;
import database.UserDao;
import database.cart;
import database.cartDao;

public class Product_Detail_Activity extends Activity implements View.OnClickListener{

    TextView title;
    String t,p,img_u,des,sp;
    TextView description;
    Button add_to_cart;
    TextView sprice, aprice;
    ImageView pro_img;
    EditText count;
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Intent i = getIntent();
         b = i.getExtras();
        NumberFormat nm = NumberFormat.getNumberInstance();
        initVariables();


         t=b.getString("IntentProductTitle")+" : ";
         p=nm.format(b.getInt("IntentProductActualPrice"));
         sp=nm.format(b.getInt("IntentProductSalePrice"));
         img_u=b.getString("IntentProductImage");
         des=b.getString("IntentProductDescription");


        title.setText(t);
        aprice.setText(p);
        sprice.setText(sp);
        description.setText(des);
        Glide.with(getApplicationContext()).load(img_u).into(pro_img);

        add_to_cart.setOnClickListener(this);

    }

    public void initVariables()
    {
        add_to_cart = (Button)findViewById(R.id.add_to_cart);
        title = (TextView)findViewById(R.id.scr2_title);
        description = (TextView)findViewById(R.id.scr2_pro_des);
        aprice = (TextView)findViewById(R.id.scr2_price);
        sprice = (TextView)findViewById(R.id.scr2_Sale_price);
        pro_img=(ImageView)findViewById(R.id.scr2_image);
        count=(EditText) findViewById(R.id.count);
    }
    @Override
    public void finish() {
        Intent i = new Intent();
        i.putExtra("isDataReceived", true);
        setResult(RESULT_OK,i);

        super.finish();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.add_to_cart)
        {

            cart c = new cart();
            c.setPro_image(img_u);
            c.setPro_des(des);
            c.setTitle(t);
            if(b.getInt("IntentProductSalePrice")!=0)
                c.setPrice(b.getInt("IntentProductSalePrice"));
            else
                c.setPrice(b.getInt("IntentProductActualPrice"));

            if(count.getText().toString().isEmpty())
            {
                c.setQuantity(1);
            }
            else
                {
                c.setQuantity(Integer.parseInt(count.getText().toString()));
            }
            c.setTotal(c.getPrice()*c.getQuantity());

            cartDao cdao = AppDatabase.getInstance(getApplicationContext()).cart_items();
            cdao.insertCartItem(c);
            Toast.makeText(getApplicationContext(), "Added to cart", Toast.LENGTH_SHORT).show();
            Intent i =new Intent(getApplicationContext(),CartActivity.class);
            startActivity(i);


        }
    }
}

