package com.example.ssc.simarket;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import database.AppDatabase;
import database.Feedback;
import database.FeedbackDao;
import database.User;
import database.UserDao;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener{

    Button submit;
    EditText nam,email,fdbck;
    CheckBox c1;

    ImageView search_img,home_img,cart_img,user_img,sale_img;
    TextView tv_home,tv_cart,tv_user,tv_sale;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);

        ActionBar appbar=getSupportActionBar();
        appbar.setTitle("Feedback...");


        initvariables();

        submit.setOnClickListener(this);

        home_img.setOnClickListener(this);
        cart_img.setOnClickListener(this);

        sale_img.setOnClickListener(this);
        user_img.setOnClickListener(this);


        tv_home.setOnClickListener(this);
        tv_cart.setOnClickListener(this);
        tv_sale.setOnClickListener(this);

        tv_user.setOnClickListener(this);


    }
    public void initvariables(){

        nam=(EditText) findViewById(R.id.edittext_name);
        email=(EditText) findViewById(R.id.edittext_email);
        fdbck=(EditText)findViewById(R.id.edittext_feedback);
        c1=(CheckBox)findViewById(R.id.email_response);
        submit=(Button)findViewById(R.id.submit);



        home_img=(ImageView)findViewById(R.id.img_home);
        cart_img=(ImageView)findViewById(R.id.img_cart);

        user_img=(ImageView)findViewById(R.id.img_user);
        sale_img=(ImageView)findViewById(R.id.img_sale);

        tv_home=(TextView)findViewById(R.id.tv_home);
        tv_cart=(TextView)findViewById(R.id.tv_cart);

        tv_sale=(TextView)findViewById(R.id.tv_sale);
        tv_user=(TextView)findViewById(R.id.tv_user);


    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    protected void sendEmail(String n, String email) {

        String emailBody="Thanks "+n+" \n Your feedback is precious for us.... ";

        String fromEmail="faryal.rajpoot121@gmail.com";
        String fromPassword="libra789libra";
        List<String> toEmailList = Arrays.asList(email
                .split("\\s*,\\s*"));
        String emailSubject="Confirmation Message";

        new SendMailTask(FeedbackActivity.this).execute(fromEmail,
                fromPassword, toEmailList, emailSubject, emailBody);
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
         if(v.getId()== R.id.submit){

             String n=nam.getText().toString();
             String e=email.getText().toString();
             String f=fdbck.getText().toString();

             if(n.isEmpty() || e.isEmpty() || f.isEmpty())
             {
                 Toast.makeText(getApplicationContext(), "plz fill Textfields.", Toast.LENGTH_LONG).show();
             }
             else if(!isValidEmail(e))
             {
                 Toast.makeText(getApplicationContext(), "plz enter correct email.", Toast.LENGTH_LONG).show();
             }
             else {
                 Feedback fobj = new Feedback();
                 fobj.setName(n);
                 fobj.setEmail(e);
                 fobj.setDetail(f);

                 FeedbackDao feed = AppDatabase.getInstance(getApplicationContext()).feedback_dao();
                 feed.insertFeedbck(fobj);
                 Toast.makeText(getApplicationContext(), "Thanks for your feedbaack.....", Toast.LENGTH_LONG).show();


                 if (c1.isChecked() && isValidEmail(e)  && isOnline()){


                      sendEmail(n,e);
                 }

             }

         }
        if(v.getId()==R.id.img_home || v.getId()==R.id.tv_home )
        {
            Intent i=new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.img_cart || v.getId()==R.id.tv_cart )
        {
            Intent i=new Intent(getApplicationContext(), CartActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.img_user || v.getId()==R.id.tv_user )
        {
            Intent i=new Intent(getApplicationContext(), MeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }


    }
}
