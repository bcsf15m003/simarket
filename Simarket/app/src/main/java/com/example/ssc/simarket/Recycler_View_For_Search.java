package com.example.ssc.simarket;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import database.AppDatabase;
import database.Product;
import database.ProductDao;

import static java.security.AccessController.getContext;

public class Recycler_View_For_Search extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProductAdapter mAdapter;
    private String pro=null;

    List<Product> pList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_for_search);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        Intent i = getIntent();
        pro = i.getStringExtra("search");

        ProductDao PDAO = AppDatabase.getInstance(getApplicationContext()).pro_dao();
        List<Product> l = PDAO.getAllProduct();

        for (int j = 0; j < l.size(); j++) {

            Product obj = l.get(j);
            if (pro != null) {
                if (pro.equals(obj.getTitle()) || pro.equals(obj.getCategory()) || pro.equalsIgnoreCase(obj.getTitle()) || pro.equalsIgnoreCase(obj.getCategory()) || obj.getTitle().contains(pro) || obj.getCategory().contains(pro)) {
                    pList.add(obj);
                }
            }
        }


        if (pList.size()!=0) {
            mAdapter = new ProductAdapter(pList);
            recyclerView.setAdapter(mAdapter);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Context context = view.getContext();
                    Product p = pList.get(position);
                    Toast.makeText(getApplicationContext(), p.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), Product_Detail_Activity.class);
                    intent.putExtra("IntentProductTitle", p.getTitle());
                    intent.putExtra("IntentProductDescription", p.getPro_des());
                    intent.putExtra("IntentProductActualPrice", p.getActual_price());
                    intent.putExtra("IntentProductSalePrice", p.getSale_price());
                    intent.putExtra("IntentProductImage", p.getPro_image());
                    startActivity(intent);


                }

                @Override
                public void onLongClick(View view, int position) {

                }
            }));


        }
        else
        {
            Toast.makeText(getApplicationContext(), "No product found", Toast.LENGTH_SHORT).show();
            Intent in=new Intent(getApplicationContext(), MainActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
        }

    }


}
