package com.example.ssc.simarket;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class SendMailTask extends AsyncTask<Object, Object, Object> {

    private ProgressDialog statusDialog;
    private Activity sendMailActivity;

    public SendMailTask(Activity activity) {
        sendMailActivity = activity;

    }

    protected void onPreExecute() {
        statusDialog = new ProgressDialog(sendMailActivity);
        statusDialog.setMessage("Getting ready...");
        statusDialog.setIndeterminate(false);
        statusDialog.setCancelable(false);
        statusDialog.show();
    }

    @Override
    protected Object doInBackground(Object... args) {
        try {
            Log.i("SendMailTask", "About to instantiate GMail...");

            GMail androidEmail = new GMail(args[0].toString(),
                    args[1].toString(), (List<String>) args[2], args[3].toString(),
                    args[4].toString());

            androidEmail.createEmailMessage();

            androidEmail.sendEmail();

            Log.i("SendMailTask", "Mail Sent.");
        } catch (Exception e) {
           publishProgress(e.getMessage());
            Log.e("SendMailTask", e.getMessage(), e);
        }
        return null;
    }


    @Override
    public void onPostExecute(Object result) {
        statusDialog.setMessage("Emil sent...");
        statusDialog.dismiss();

    }

}