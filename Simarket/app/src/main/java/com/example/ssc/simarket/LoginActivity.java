package com.example.ssc.simarket;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import database.AppDatabase;
import database.User;
import database.UserDao;


public class LoginActivity extends Activity implements View.OnClickListener{

    EditText uname,pswd;
    Button loggin_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initVariables();
        loggin_btn.setOnClickListener(this);




    }

    /**
     * Called when the user taps the register button
     */


    @Override
    public void onClick(View v) {
        String u = uname.getText().toString();
        String p = pswd.getText().toString();
        Boolean flag = false;
        UserDao uobj = (UserDao) AppDatabase.getInstance(getApplicationContext()).user();
        User user= uobj.getUser(u,p);
        if(user!=null && (user.getEmail().equals(u) || user.getEmail().equalsIgnoreCase(u)) && user.getPassword().equals(p))
        {
            SharedPreferences sp =  getSharedPreferences("User_session", Context.MODE_PRIVATE);


                SharedPreferences.Editor editor = sp.edit();
                editor.putString("uname", u);
                editor.putString("pswd", p);
                editor.apply();
            Toast.makeText(getApplicationContext(), "User logged In", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getApplicationContext(), MeActivity.class);
            i.putExtra("flag",false);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);


        }
        else
        {
            Toast.makeText(getApplicationContext(), "Logged in failed", Toast.LENGTH_SHORT).show();
        }

    }
    public void getRegister(View view) {
        Intent i = new Intent(getApplicationContext(), SignupActivity.class);
        i.putExtra("flag",false);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public void initVariables()
    {
        loggin_btn = (Button)findViewById(R.id.login);
        uname= (EditText)findViewById(R.id.uname);
        pswd = (EditText)findViewById(R.id.pswd);

    }
}