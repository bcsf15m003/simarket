package com.example.ssc.simarket;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import database.AppDatabase;
import database.Product;
import database.ProductDao;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private RelativeLayout rl1;
    Button login,signup;
    EditText searched_pro;
   ImageView search_img,home_img,cart_img,user_img,sale_img;
   TextView tv_home,tv_cart,tv_user,tv_sale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PopulateData();
        initVariables();

        search_img.setOnClickListener(this);
        home_img.setOnClickListener(this);
        cart_img.setOnClickListener(this);
        sale_img.setOnClickListener(this);
        user_img.setOnClickListener(this);

        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        tv_home.setOnClickListener(this);
        tv_cart.setOnClickListener(this);
        tv_sale.setOnClickListener(this);

        tv_user.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.login_btn_)
        {
            Intent i=new Intent(getApplicationContext(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.signup_btn_)
        {
            Intent i=new Intent(getApplicationContext(), SignupActivity.class);
            i.putExtra("flag",false);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.img_search)
        {
            String s= searched_pro.getText().toString();
            if(!s.isEmpty()) {
                Intent i = new Intent(getApplicationContext(), Recycler_View_For_Search.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("search", s);
                startActivity(i);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Searched string can't be empty", Toast.LENGTH_SHORT).show();
            }
        }
        if(v.getId()==R.id.img_home || v.getId()==R.id.tv_home )
        {
            Intent i=new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.img_cart || v.getId()==R.id.tv_cart )
        {
            Intent i=new Intent(getApplicationContext(), CartActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        if(v.getId()==R.id.img_user || v.getId()==R.id.tv_user )
        {
            Intent i=new Intent(getApplicationContext(), MeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }



    }

    public void initVariables()
    {
        login=(Button)findViewById(R.id.login_btn_);
        signup=(Button)findViewById(R.id.signup_btn_);
        search_img=(ImageView)findViewById(R.id.img_search);
        searched_pro=(EditText)findViewById(R.id.search_text);


        home_img=(ImageView)findViewById(R.id.img_home);
        cart_img=(ImageView)findViewById(R.id.img_cart);

        user_img=(ImageView)findViewById(R.id.img_user);
        sale_img=(ImageView)findViewById(R.id.img_sale);

        tv_home=(TextView)findViewById(R.id.tv_home);
        tv_cart=(TextView)findViewById(R.id.tv_cart);
        tv_sale=(TextView)findViewById(R.id.tv_sale);
        tv_user=(TextView)findViewById(R.id.tv_user);


    }

    public void PopulateData()
    {
        ProductDao ProductDAO = AppDatabase.getInstance(getApplicationContext()).pro_dao();
        Product p1 = new Product("paracetamol",10,"https://images.jumpseller.com/store/simmarket/3069421/paracetamol.jpg?1555865638","Medicine",0,false,"Good for headache");
        ProductDAO.insertProduct(p1);

        Product p2 = new Product("coke",80,"https://images.jumpseller.com/store/simmarket/3070218/coke.jpg?0","Cold Drinks",65,true,"For refreshment");
        ProductDAO.insertProduct(p2);

        Product p3 = new Product("Fanta",80,"https://images.jumpseller.com/store/simmarket/3070222/fanta.jpg?0","Cold Drinks",65,true,"For refreshment");
        ProductDAO.insertProduct(p3);
        Product p4 = new Product("Sprite",80,"https://images.jumpseller.com/store/simmarket/3070224/sprite.jpg?1555865441","Cold Drinks",65,true,"For refreshment");
        ProductDAO.insertProduct(p4);


        Product p5 = new Product("Vivo",22000,"https://images.jumpseller.com/store/simmarket/3070227/vivo.jpg?1555865856","Electronics",20000,true,"Best hardware and software");
        ProductDAO.insertProduct(p5);
        Product p6 = new Product("Pink frok",2000,"https://images.jumpseller.com/store/simmarket/3070230/pink_frok.jpg?0","Baby froke.",1500,true,"For Babies");
        ProductDAO.insertProduct(p6);

        Product p7 = new Product("Blue frok",2000,"https://images.jumpseller.com/store/simmarket/3070231/blue_frok.jpg?0","Baby froke.",1500,true,"For Babies");
        ProductDAO.insertProduct(p7);



        Product p8 = new Product("IPhone",115000,"https://images.jumpseller.com/store/simmarket/3070232/iphone.jpeg?0","Electronics",0,false,"Best hardware and software");
        ProductDAO.insertProduct(p8);
        Product p9 = new Product("CauliFlower",20,"https://images.jumpseller.com/store/simmarket/3070233/cauliflower.jpg?0","Vegetable", 0,false,"Good for health");
        ProductDAO.insertProduct(p9);

        Product p10 = new Product("Capsicum",20,"https://images.jumpseller.com/store/simmarket/3070236/capcicum.jpg?0","Vegetable", 0,false,"Good for health");
        ProductDAO.insertProduct(p10);



    }

}
