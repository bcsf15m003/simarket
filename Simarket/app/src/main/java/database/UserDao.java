package database;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

    @Query("Select * from User where email= :e And password=:p")
    public User getUser(String e, String p);

   @Query("Select * from User")
   public List<User> getAllUser();

    @Delete
    public void deleteUser(User user);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertUser(User user);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void UpdateUser(User u);

}
