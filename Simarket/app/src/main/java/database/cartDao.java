package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface cartDao {

    @Query("Select * from cart")
    public List<cart> getAllcartItems();

    @Delete
    public void deleteCartItem(cart c);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCartItem(cart c);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void UpdateCart(cart c);





}
