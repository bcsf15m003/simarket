package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class cart {


    @PrimaryKey(autoGenerate = true)
    private int cart_id;

    @NonNull
    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(@NonNull int cart_id) {
        this.cart_id = cart_id;
    }

    private String  title;
    private int  price;
    private String  pro_image ;
    private String  pro_des;
    private int quantity;
    private int total;




    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPro_image() {
        return pro_image;
    }

    public String getPro_des() {
        return pro_des;
    }

    public void setPro_des(String pro_des) {
        this.pro_des = pro_des;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
