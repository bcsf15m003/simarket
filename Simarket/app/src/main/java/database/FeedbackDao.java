package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface FeedbackDao {

    @Delete
    public void deleteFeedbck(Feedback fobj);


    @Insert(onConflict = REPLACE)
    public void insertFeedbck(Feedback f);

}
