package database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Product {

    @PrimaryKey
    @NonNull
    private String  title;
    private int  actual_price;
    private String  pro_image ;
    private String category;
    private int sale_price;
    private boolean is_on_sale;
    private String  pro_des;


    public Product(String title, int actual_price, String pro_image, String category,int sale_price, boolean is_on_sale, String pro_des){

      this.title=title;
      this.actual_price=actual_price;
      this.pro_image=pro_image;
      this.category=category;
      this.sale_price=sale_price;
      this.is_on_sale=is_on_sale;
      this.pro_des=pro_des;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }


    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPro_des() {
        return pro_des;
    }

    public boolean isIs_on_sale() {
        return is_on_sale;
    }

    public void setIs_on_sale(boolean is_on_sale) {
        this.is_on_sale = is_on_sale;
    }

    public void setPro_des(String pro_des) {
        this.pro_des = pro_des;
    }

    public int getSale_price() {
        return sale_price;
    }

    public void setSale_price(int sale_price) {
        this.sale_price = sale_price;
    }

    public int getActual_price() {
        return actual_price;
    }

    public void setActual_price(int actual_price) {
        this.actual_price = actual_price;
    }
}
