package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

@Dao
public interface ProductDao {

    @Query("Select * from Product where title= :t")
    public Product getProduct(String t);

    @Query("Select * from Product")
    public List<Product> getAllProduct();

    @Delete
    public void deleteProduct(Product p);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertProduct(Product p);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void UpdateProduct(Product p);

}
