package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface checkoutDao {
    @Query("Select * from checkout")
    public List<checkout> getCheckoutDetail();

    @Delete
    public void deleteCheckout(checkout c);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertCheckoutDetail(checkout c);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public void UpdateCheckoutDetail(checkout c);





}
